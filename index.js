let balance = 0;
let outstanding = 0;
let boughtComputer = true;
let payAmount = 0;

const outstandingText = document.getElementById("outstanding");
const outstandingValueText = document.getElementById("outstandingValue");
const balanceValue = document.getElementById("balanceValue");

const repayButton = document.getElementById("repayButton");
const payValueText = document.getElementById("payValue");

const dropdown = document.getElementById("laptopsDropdown");
const features = document.getElementById("featuresText");

const image = document.getElementById("computerImage");
const adTitle = document.getElementById("computerTitle");
const adDescription = document.getElementById("computerDescription");
const adPrice = document.getElementById("computerPrice");


// Happens when the "Get a loan" button is clicked
function clickLoan() {
    // User input of loan amount
    let loanAmount = prompt("Please enter amount", 0)
    loanAmount = Number(loanAmount);

    // Requirements for getting a loan
    if (loanAmount > 2*balance) {
        alert("You cannot get a loan more than double your bank balance.")
    } else if (outstanding > 0) {
        alert("You must pay back your previous loan before getting another loan.")
    } else if (!boughtComputer) {
        alert("You must buy a computer before getting another loan.")
    } else {
        // Loan is approved
        if (loanAmount > 0) {
            outstanding = loanAmount;
            balance += loanAmount;
            boughtComputer = false;
            // Updates the user
            outstandingText.style.opacity = "1";
            outstandingValueText.style.opacity = "1";
            outstandingValueText.innerText = outstanding + "kr";
            balanceValue.innerText = balance + "kr";
            repayButton.style.opacity = "1";
        }
    }
}

// Happens when the "Bank" button is clicked
function clickBank() {
    // Automatically repays 10% of the loan (if any loan)
    if (outstanding > 0) {
        // Loan is payed back
        if (payAmount*0.1 > outstanding) {
            balance += (payAmount - outstanding);
            outstanding = 0;

            outstandingText.style.opacity = 0;
            outstandingValueText.style.opacity = 0;
            repayButton.style.opacity = 0;
        } else {
            outstanding -= (payAmount * 0.1);
            balance += (payAmount * 0.9);
        }
        outstandingValueText.innerText = outstanding + "kr";
    } else {
        balance += payAmount;
    }
    payAmount = 0;
    // Updates the user
    payValueText.innerText = payAmount + "kr";
    balanceValue.innerText = balance + "kr";
}

// Happens when the "Work" button is clicked
function clickWork() {
    // Increases the current pay by 100kr
    payAmount += 100;
    payValueText.innerText = payAmount + "kr";
}

// Happens when the "Repay Loan" button is clicked
function clickRepay() {
    // Repays the pay amount to the loan
    if (outstanding > payAmount) {
        outstanding -= payAmount;
    } else {
        // If loan is less than the pay, the rest of the pay goes to the balance
        payAmount -= outstanding;
        outstanding = 0;
        balance += payAmount;

        outstandingText.style.opacity = 0;
        outstandingValueText.style.opacity = 0;
        repayButton.style.opacity = 0;
    }
    payAmount = 0;
    // Updates the user
    payValueText.innerText = payAmount + "kr";
    balanceValue.innerText = balance + "kr";
    outstandingValueText.innerText = outstanding + "kr";
}

// URL of the JSON file containing computers
const baseURL = "https://noroff-komputer-store-api.herokuapp.com/";

// Fetches data from URL and processes the data
async function selectComputers() {

    fetch(baseURL + "computers/")
        // Processes the JSON data
        .then( data => data.json())
        .then( data => addToDropdown(data))
        .catch( error => console.error(error));
}

selectComputers();

let jsondata;

// Adds the titles from the JSON file to the dropdown menu
function addToDropdown(data) {

    jsondata = data;
    for (computer of data) {
        let item = document.createElement("option");
        item.innerText = computer.title;
        dropdown.appendChild(item);
    }
    updateFeatures(jsondata[0]);
    updateAd(jsondata[0]);
}

// Updates the features when the chosen computer changes
dropdown.addEventListener("change", event => {

    for (computer of jsondata) {
        if (computer.title === event.target.value) {
            updateFeatures(computer);
            updateAd(computer);
        }
    }
})

// To update the features field with the features of the current chosen computer
function updateFeatures(computer) {
    // Clears the previous features
    features.innerHTML = "";
    // Adds all features of the current computer
    for (spec of computer.specs) {
        const feature = document.createElement("div");
        feature.innerText = spec;
        features.appendChild(feature);
    }
}

// Updates the AD box
function updateAd(computer) {
    // Clears the fields
    adDescription.innerHTML = "";
    adPrice.innerHTML = "";
    adTitle.innerHTML = "";
    image.innerHTML = "";

    // Inserts the info for the current chosen computer
    image.src = baseURL + computer.image;
    adTitle.innerText = computer.title;
    adDescription.innerText = computer.description;
    adPrice.innerText = computer.price + "kr";
}

function clickBuy() {
    let comp = jsondata[0];
    for (computer of jsondata) {
        if (computer.title === dropdown.options[dropdown.selectedIndex].value) {
            comp = computer;
        }
    }

    const price = comp.price;
    console.log(price);

    if (price <= balance) {
        balance -= price;
        boughtComputer = true;
        balanceValue.innerText = balance + "kr";
        alert(`You are now the owner of ${comp.title}, congratulations!`)
    } else {
        alert("You cannot afford this laptop.");
    }
}
